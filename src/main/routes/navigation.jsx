import { Link } from "react-router-dom";
import "../../resources/styles/navigation.scss";

const Navigation = () => {
    return (
        <div>
            <nav className="nav">
                <ul className="nav-list">
                    <li className="nav-list-item">
                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    <li className="nav-list-item">
                        <Link to="/posts" className="nav-link">Posts</Link>
                    </li>
                    <li className="nav-list-item">
                        <Link to="/todos" className="nav-link">Todos</Link>
                    </li>
                    <li className="nav-list-item">
                        <Link to="/users" className="nav-link">Users</Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Navigation;