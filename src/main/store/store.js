import { configureStore } from "@reduxjs/toolkit";
import counterReducers from "../../library/common/components/reduxCounter/counterReducer";
import postsReducer from "../../modules/postsList/postsReducer";
import todosReducer from "../../modules/todoList/todosReducer";
import userDetailsReducer from "../../modules/userDetails/userDetailsReducer";
import usersReducer from "../../modules/usersList/usersReducer";

const store = configureStore({
  reducer: {
    counter: counterReducers,
    users: usersReducer,
    posts: postsReducer,
    todos: todosReducer,
    user: userDetailsReducer,
  },
});

export default store;
