import React from "react";
import Navigation from "./main/routes/navigation";
import { Routes, Route } from "react-router-dom";
import "./resources/styles/App.scss";
import PostsList from "./modules/postsList/postsList";
import TodoList from "./modules/todoList/todoList";
import UserList from "./modules/usersList/userList";
import Home from "./modules/home/home";
import UserDetails from "./modules/userDetails/userDetails";

function App() {
  return (
    <div className="app">
      <header className="app-header">
        <Navigation />
      </header>

      <main className="app-main">
        <Routes>
          <Route path="/posts" element={<PostsList />} />
          <Route path="/todos" element={<TodoList />} />
          <Route path="/users" element={<UserList />} />
          <Route path="/users/:userId" element={<UserDetails />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </main>
    </div>
  );
}

export default App;
