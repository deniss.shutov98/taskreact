import { useEffect, useState } from "react";
import "./popup.scss";
import PropTypes from "prop-types";

const Popup = (props) => {
    const [show, setShow] = useState(false);

    const closeHandler = () => {
        setShow(false);
        props.onClose();
    };

    useEffect(() => {
        setShow(props.show);
    }, [props.show]);

    return (
        <div
            style={{
                visibility: show ? "visible" : "hidden",
                opacity: show ? "1" : "0"
            }}
            className="overlay"
            >
            <div className="popup">
                <h2>{props.title}</h2>
                <span className="close" onClick={closeHandler}>
                &times;
                </span>
                <div className="content">{props.children}</div>
            </div>
        </div>
    );
}

Popup.propTypes = {
    title: PropTypes.any.isRequired,
    show: PropTypes.any.isRequired,
    onClose: PropTypes.any.isRequired
  };

export default Popup;