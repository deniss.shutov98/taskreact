import React from "react";
import "../../styles/counterStyles/counter.scss";

class ClassCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = { counter: 0 };
  }

  render() {
    return (
      <>
        <h1>Class Counter</h1>
        <div className="counter-container">
          <button
            className="button"
            onClick={() => this.setState({ counter: this.state.counter - 1 })}
          >
            -
          </button>
          <label className="label">{this.state.counter}</label>
          <button
            className="button"
            onClick={() => this.setState({ counter: this.state.counter + 1 })}
          >
            +
          </button>
        </div>
      </>
    );
  }
}

export default ClassCounter;
