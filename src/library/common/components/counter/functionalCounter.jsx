import React, { useState } from "react";
import "../../styles/counterStyles/counter.scss";


function FunctionalCounter() {
  const [counter, setCounter] = useState(0);

  return (
    <>
      <h1>Functional Counter</h1>
      <div className="counter-container">
        <button className="button" onClick={() => setCounter(counter - 1)}>
          -
        </button>
        <label className="label">{counter}</label>
        <button className="button" onClick={() => setCounter(counter + 1)}>
          +
        </button>
      </div>
    </>
  );
}

export default FunctionalCounter;
