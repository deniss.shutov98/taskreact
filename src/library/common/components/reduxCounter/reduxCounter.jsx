import React from "react";
import { useSelector, useDispatch } from 'react-redux'
import { increment, decrement } from '../../reducers/counterReducer'
import "../../styles/counterStyles/counter.scss";



function ReduxCounter() {
    const count = useSelector(state => state.counter.count)
    const dispatch = useDispatch()

    return (
    <>
        <h1>Redux Counter</h1>
        <div className="counter-container">
        <button className="button" onClick={() => dispatch(decrement())}>
            -
        </button>
        <label className="label">{count}</label>
        <button className="button" onClick={() => dispatch(increment())}>
            +
        </button>
        </div>
    </>
    );
}

export default ReduxCounter;
