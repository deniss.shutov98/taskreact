import { createSlice } from "@reduxjs/toolkit";
import useFetchTodos from "./useFetchTodos";

const todosSlice = createSlice({
  name: "todos",
  initialState: {
    todos: [],
  },
  reducers: {
    setTodos(state, action) {
      state.todos = action.payload;
    },
    toggleTodo(state, action) {
      let targetItem = action.payload;
      let stateTodo = state.todos.find((x) => x.id === targetItem.id);
      stateTodo.completed = !stateTodo.completed;
    },
  },
});

export async function useLoadTodos(dispatch) {
  const response = await useFetchTodos();
  dispatch(setTodos(response));
}

export const { setTodos, toggleTodo } = todosSlice.actions;
export default todosSlice.reducer;
