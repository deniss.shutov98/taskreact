import "./todoList.scss"
import React from "react"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react";
import { useLoadUsers } from "../usersList/usersReducer";
import { useLoadTodos, toggleTodo } from "./todosReducer";

const TodoList = () => {
    const todos = useSelector(state => state.todos.todos);
    const users = useSelector(state => state.users.users);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(useLoadUsers);
        dispatch(useLoadTodos);
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getUserName = (userId) => {
        return users?.find(x => x.id === userId)?.name || 'unknown';
    }

    const todoOnClick = (value) => {
        dispatch(toggleTodo(value));
    }

    return (
    <div className="todos-container text">
        <h1 className="todo-header">TODOs List</h1>
        {todos.map((value) => {
            let completed = value.completed ? 'Done!' : 'Not done';
            return (
                <div className="todo-container" key={ `todo${value.id}`} 
                onClick={() => todoOnClick(value)}>
                    <h2>{value.title}</h2>
                    <label>Author: {getUserName(value.userId)}</label><br/>
                    <label>{completed}</label>
                </div>
            )
        })}
    </div>
    )
}

export default TodoList;