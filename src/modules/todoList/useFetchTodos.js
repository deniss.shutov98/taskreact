import axios from "axios";

const useFetchTodos = async () => {
  return await axios
    .get("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.data)
    .catch((error) => console.log(error));
};

export default useFetchTodos;
