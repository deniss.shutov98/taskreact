import axios from "axios";

export const useFetchPosts = async () => {
  return await axios
    .get("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.data)
    .catch((error) => console.log(error));
};

export const useAddPost = async (data) => {
  return await axios
    .post("https://jsonplaceholder.typicode.com/posts", {
      userId: data.userId,
      title: data.title,
      body: data.body,
    })
    .then((response) => {
      return response;
    })
    .catch((error) => console.log(error));
};
