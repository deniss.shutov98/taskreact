import { createSlice } from "@reduxjs/toolkit";
import { useFetchPosts } from "./useFetchPosts";

const postsSlice = createSlice({
  name: "posts",
  initialState: {
    posts: [{ title: "initialPost" }],
  },
  reducers: {
    setPosts(state, action) {
      state.posts = action.payload;
    },
    addPost(state, action) {
      state.posts.push(action.payload);
    },
  },
});

export async function useLoadPosts(dispatch) {
  const response = await useFetchPosts();
  dispatch(setPosts(response));
}

export const { setPosts, addPost } = postsSlice.actions;
export default postsSlice.reducer;
