import "./forms.scss"
import React from "react";
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from "react-hook-form";
import { addPost } from "../postsReducer";
import { useAddPost } from "../useFetchPosts";
import PropTypes from "prop-types";

const AddPostForm = (props) => {
    const users = useSelector(state => state.users.users);
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    
    const useOnSubmit = async (data) => {
        let response = await useAddPost(data);
        if(response.status === 201) {
            dispatch(addPost({
                userId: parseInt(response.data.userId),
                id: response.data.id,
                title: response.data.title,
                body: response.data.body
            }))
            props.closeOnSubmit();
        }
    };


    return (
        <form className="form" onSubmit={handleSubmit(useOnSubmit)}>
            <select {...register("userId")} className="input">
                {
                    users.map(x => {
                        return (
                            <option defaultValue={1} value={x.id} key={x.id}>{x.name}</option>
                        )
                    })
                }
            </select>
            <input {...register("title")} className="input" placeholder="Title"/>
            <input {...register("body")} className="input" placeholder="Body"/>
            <input type="submit" value="Submit" className="input"/>
        </form>
    )
}

AddPostForm.propTypes = {
    closeOnSubmit: PropTypes.any.isRequired
  };

export default AddPostForm;