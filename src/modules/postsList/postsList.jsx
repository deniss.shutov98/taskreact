import React from "react"
import "./postsList.scss"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from "react";
import { useLoadUsers } from "../usersList/usersReducer";
import { useLoadPosts } from "./postsReducer";
import Popup from "../../library/common/components/popup/popup";
import AddPostForm from "./forms/addPostForms";
import { v4 as uuidv4 } from 'uuid';

const PostsList = () => {
    const posts = useSelector(state => state.posts.posts);
    const users = useSelector(state => state.users.users);
    const [visibility, setVisibility] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(useLoadUsers);
        dispatch(useLoadPosts);
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getUserName = (userId) => {
        return users?.find(x => x.id === userId)?.name || 'unknown';
    }

    const openAddPostForm = () => {
        setVisibility(true)
    }

    function popupCloseHandler() {
        setVisibility(false);
      };


    return (
        <div className="posts-container text">
            <header className="post-header">
                <h1>Posts List</h1>
                <button className="add-post-button" onClick={openAddPostForm}>Add Post</button>
            </header>
            
            {posts.map((value) => {
                return(
                    <div className="post-container" key={uuidv4()}>
                        <h2>{value.title}</h2>
                        <label>Author: {getUserName(value.userId)}</label>
                        <p>{value.body}</p>
                    </div>
                )
            })}

            <Popup
                onClose={popupCloseHandler}
                show={visibility}
                title="Add Post">
                <AddPostForm closeOnSubmit={popupCloseHandler}/>
            </Popup>
        </div>
    )
} 

export default PostsList;