import "./usersList.scss"
import React from "react"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react";
import { useLoadUsers } from "./usersReducer";
import { Link } from "react-router-dom";

const UserList = () => {
    const users = useSelector(state => state.users.users);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(useLoadUsers);
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return (
        <div className="users-container text">
            <div className="users">
                <ul className="users-list">
                {users.map((value) => {
                    return (
                        <li key={`user${value.id}`} className="list-item" >
                            <Link to={value.id.toString()} className="link-item">
                                {value.name} `{value.username}`
                            </Link>
                        </li>
                    )
                })}
                </ul>
            </div>
        </div>
    )
}

export default UserList;