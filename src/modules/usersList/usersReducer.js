import { createSlice } from "@reduxjs/toolkit";
import { useFetchUsers } from "./useFetchUsers";

const usersSlice = createSlice({
  name: "users",
  initialState: {
    users: [],
  },
  reducers: {
    setUsers(state, action) {
      state.users = action.payload;
    },
  },
});

export async function useLoadUsers(dispatch, getState) {
  const response = await useFetchUsers();
  dispatch(setUsers(response));
}

export const { setUsers } = usersSlice.actions;
export default usersSlice.reducer;
