import axios from "axios";

export const useFetchUsers = async () => {
  return await axios
    .get("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.data)
    .catch((error) => console.log(error));
};
