import axios from "axios";

export const useFetchUserByIdAsync = async (id) => {
  return await axios
    .get(`https://jsonplaceholder.typicode.com/users/${id}`)
    .then((response) => response.data)
    .catch((error) => console.log(error));
};
