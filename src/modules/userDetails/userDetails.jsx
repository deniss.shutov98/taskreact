import "./userDetails.scss"
import { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux'
import { useParams } from "react-router-dom";
import { IoMail, IoLocationSharp, IoCallSharp, IoCameraSharp, IoAnalyticsSharp } from "react-icons/io5";
import { getUserById } from "./userDetailsReducer";


const UserDetails = () => {
    const user = useSelector(state => state.user.user);
    const { userId } = useParams();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserById(userId))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId])

    const getAddress = (address) => {
        if(address === undefined) return ``;
        return `${address.street}, ${address.city}, ${address.zipcode}`;
    }

    const getCatchPhrase = (company) => {
        if(company === undefined) return ``;
        return company.catchPhrase;
    }

    return (
        <div className="container">
            <div className="item">
                <IoMail className="icon"/>
                <label>{user?.email}</label>
            </div>
            <div className="item">
                <IoCallSharp className="icon"/>
                <label>{user?.phone}</label>
            </div>
            <div className="item">
                <IoLocationSharp className="icon"/>
                <label>{getAddress(user?.address)}</label>
            </div>
            <div className="item">
                <IoCameraSharp className="icon"/>
                <label>{`@${user?.username}`}</label>
            </div>
            <div className="item">
                <IoAnalyticsSharp className="icon"/>
                <label>{getCatchPhrase(user?.company)}</label>
            </div>
        </div>
    )
}

export default UserDetails;