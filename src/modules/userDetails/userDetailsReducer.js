import { createSlice } from "@reduxjs/toolkit";
import { useFetchUserByIdAsync } from "./useFetchUser";

const usersSlice = createSlice({
  name: "userDatails",
  initialState: {
    user: {},
  },
  reducers: {
    setUser(state, action) {
      state.user = action.payload;
    },
  },
});

export function getUserById(id) {
  return async function useLoadUser(dispatch, getState) {
    const response = await useFetchUserByIdAsync(id);
    console.log(response);
    dispatch(setUser(response));
  };
}

export const { setUser } = usersSlice.actions;
export default usersSlice.reducer;
